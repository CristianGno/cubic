<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('comment_id');
            $table->string('author_name');
            $table->string('author_email');
            $table->string('author_url')->nullable();
            $table->timestamp('published_at');
            $table->text('body');
            $table->tinyInteger('approved')->default('1');
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();

            $table->foreign('comment_id')
                    ->references('id')
                    ->on('comments')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
