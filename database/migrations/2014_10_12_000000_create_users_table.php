<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('biography')->nullable();
            $table->string('facebook', 60)->nullable()->default('https://facebook.com');
            $table->string('twitter', 40)->nullabel()->default('https://twitter.com');
            $table->string('github', 40)->nullable()->default('https://github.com');
            $table->string('google_plus')->nullable()->default('https://plus.google.com');
            $table->string('confirmation_token', 22);
            $table->tinyInteger('confirmed')->nullable()->default(NULL);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
