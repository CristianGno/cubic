<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {

	$body = '';
	for ($i=0; $i < 5 ; $i++) { 
		$p = '';

		for ($j=0; $j < 10 ; $j++) { 
			$p .= $faker->paragraph . ' ';
		}

		$body .= '<p>' . $p . '</p>';
	}

    return [
        'title' => $faker->sentence,
        'body' =>  $body,
        'excerpt' => str_replace('<p>', '', substr($body, 0, rand(30, 45))). '...',
        'published_at' => Carbon::yesterday(),
        'category_id' => rand(1,15),
        'user_id' => rand(1,20)

    ];
});
