<?php

use App\Comment;
use App\Response;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 25; $i++) { 
            $correos               = ['gmail', 'hotmail', 'yahoo'];
            $comment               = new Comment;
            $comment->post_id      = rand(1,20);
            $comment->author_name  = str_random(8) . ' ' . str_random(10);
            $comment->author_email = str_random(rand(8, 15)) . '@' . $correos[rand(0,2)] . '.com';
            $comment->author_url   = str_random(12) . '.com';
            $comment->published_at = Carbon::now();
            $comment->body         = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet architecto dolore ab deserunt cumque veritatis voluptatem pariatur quisquam provident, quis quibusdam impedit neque fugiat sunt dignissimos autem. Delectus, sequi, illo.';
            $comment->approved     = 1;
            $comment->save();
        }

        for ($i=0; $i < 25; $i++) { 
            $correos                = ['gmail', 'hotmail', 'yahoo'];
            $response               = new Response;
            $response->comment_id   = rand(1,25);
            $response->author_name  = str_random(8) . ' ' . str_random(10);
            $response->author_email = str_random(rand(8, 15)) . '@' . $correos[rand(0,2)] . '.com';
            $response->author_url   = str_random(12) . '.com';
            $response->published_at = Carbon::now();
            $response->body         = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet architecto dolore ab deserunt cumque veritatis voluptatem pariatur quisquam provident, quis quibusdam impedit neque fugiat sunt dignissimos autem. Delectus, sequi, illo.';
            $response->approved     = 1;
            $response->save();
        }
    }
}
