<?php
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
// use Illuminate\Support\Facades\Storage;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Storage::disk('public')->deleteDirectory('posts');
        Storage::disk('public')->deleteDirectory('uploads');

        Post::truncate();
        Category::truncate();
        Tag::truncate();
        

        $tags       = factory('App\Tag', 15)->create();
        $categories =  factory('App\Category', 15)->create();

        for ($i=0; $i < 20 ; $i++) { 
            $post = factory('App\Post')->create();
            for($j=1; $j < rand(1,4); $j++){
                $post->tags()->attach($j);
            }
        }
    }
}
