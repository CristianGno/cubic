<?php

use App\Photo;
use App\Post;
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Role::truncate();
        User::truncate();
        Post::truncate();
        Photo::truncate();*/

        $adminRole       = Role::create(['name' => 'Admin']);
        $writterRole     = Role::create(['name' => 'Writter']);

        $viewPostsPermission = Permission::create(['name' => 'View posts']);
        $editPostsPermission = Permission::create(['name' => 'Update posts']);
        $deletePostsPermission = Permission::create(['name' => 'Delete posts']);
        $createPostsPermission = Permission::create(['name' => 'Create posts']);
        
        $viewUsersPermission = Permission::create(['name' => 'View users']);
        $editUsersPermission = Permission::create(['name' => 'Update users']);
        $deleteUsersPermission = Permission::create(['name' => 'Delete users']);
        $createUsersPermission = Permission::create(['name' => 'Create users']);


        $admin           = new User;
        $admin->name     = "Cristian";
        $admin->email    = "cristian.galeano1913@gmail.com";
        $admin->password = "1234";
        $admin->confirmation_token = str_random(22);
        $admin->biography = "Desarrollador Web PHP, Laravel";
        $admin->save();

        $admin->assignRole($adminRole);

        foreach()

        /*for($i = 0; $i < 19; $i++){
          $user = factory('App\User')->create();
          $user->assignRole($writterRole);
        }*/
    }
}
