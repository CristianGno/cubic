
<div class="post_comment_area style2" id="comments">

    <h2>
        {!! comments_number($comments->count()) !!}
    </h2>
 @if ($comments->count() != 0)
   @foreach ($comments as $comment)
   
     <div class="single_comment" id="comment-{{ $comment->id }}">
        <div class="comment">
            <div class="coment_text">
                <div class="author-meta">
                    <a href="#" class="author_picture">
                        @if ($user = $comment->user)
                            @if ($picture = $user->picture)
                                <img alt="" src="{{ url($picture->url) }}">
                            @else 
                                <img alt="" src="/theme/images/blog/client4.png">
                            @endif
                        @else
                          <img alt="" src="/theme/images/blog/client4.png">
                        @endif
                    </a>
                    <div>
                        <h2><a href="#">{{ $comment->author_name }}</a></h2>
                        <span>
                            {{ $comment->published_at->format('d/m/Y h:i:s') }}</span>
                            <a id="comment-reply"  aria-commenter-name="{{ $comment->author_name }}" aria-comment-id="{{ $comment->id}}" class="fa  fa-reply" href="#comment-form"></a>
                    </div>
                </div>
                <div class="author-replay">
                    {{ $comment->body }}
                </div>
            </div>
        </div>
     @if ($comment->responses->count() > 0)
        @foreach ($comment->responses as $response)
           @if ($response->isApproved($response))
            <div class="comment comment_replay" id="comment-{{ $response->id }}">
                <div class="coment_text">
                    <div class="author-meta">
                        <a href="#" class="author_picture">
                            <img alt="" src="/theme/images/blog/client4.png">
                        </a>
                        <div>
                            <h2><a href="#">{{ $response->author_name }}</a></h2>
                            <span>{{ $response->published_at->format('d/m/Y h:i:s') }}</span>
                            <a id="comment-reply-response" class="fa  fa-reply" href="#comment-form" aria-comment-id="{{ $comment->id}}"
                                aria-commenter-name="{{ $response->author_name }}">
                                
                            </a>
                        </div>
                    </div>
                    <div class="author-replay">
                        {{ $response->body }}
                    </div>
                </div>
            </div><!-- /.comment comment_replay -->
           @endif

        @endforeach
     @endif
    </div> <!-- /.single_comment -->
  @endforeach      
@endif

    <hr>

@include('posts.partials.comment-form')
</div>