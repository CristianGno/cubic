<div class="post-meta">
    <span>
        <i class="fa fa-calendar-o"></i>
        {{ $post->published_at ? $post->published_at->format('M d') : '' }}
    </span>
    by
    <span><a href="#">{{ $post->owner['name'] }}</a></span> |
    @if ($post->category != NULL)
        <span>
          <i class="fa fa-archive"></i> 
          <a href="{{ route('categories.show', $post->category) }}">
            {{ $post->category->name }} 
          </a>
        </span> |
    @endif
    @if ($post->tags->count() > 0)
      <span><i class="fa fa-tag"></i> 
        @foreach ($post->tags as $tag)
            <a href="{{ route('tags.show', $tag) }}">
              #{{ $tag->name }}
            </a>
        @endforeach
      </span>
      |
      @endif
      <span>
        <i class="fa fa-comments"></i>
          <a href="{{ route('blog.single', $post) . '#comments' }}">
            {!! comments_number($post->comments->count()) !!}
          </a>
      </span>
    
    
</div>