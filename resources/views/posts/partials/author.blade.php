@php
	if(!$owner = $post->owner):
		return;
	endif;	
@endphp

<div class="about_psot_author">
    @if ($picture = $owner->picture)
    	<img alt="" src="{{ url($picture->url) }}">
    @else
    	<img alt="" src="/theme/images/blog/client4.png">
    @endif
    <div>
        <h2>Acerca del autor</h2>
        <h3><a href="#">{{ $post->owner->name }}</a></h3>
        <p>{{ $post->owner->biography }}</p>

        <div class="social-sharer">
            <ul class="social-links">
                <li><a href="{{ $post->owner->facebook }}"><i class="fa fa-facebook"></i></a></li>
                <li><a href="{{ $post->owner->twitter }}"><i class="fa fa-twitter"></i></a></li>
                <li><a href="{{ $post->owner->github }}"><i class="fa fa-github"></i></a></li>
                <li><a href="{{ $post->owner->google_plus }}"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div>
    </div>
</div>