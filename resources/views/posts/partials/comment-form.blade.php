    <h3 id="comments-title">
        Deja un comentario
    </h3>

<div id="comment-form" class="comment_form_box">
    <div id="message">
        
    </div>
    <form id="form-comments" class="form-horizontal" action="{{ route('comments.store') }}" method="POST">
    	{{ csrf_field() }}
 	   <div class="row"> 
    	<input type="hidden" name="post_id" value="{{ $post->id }}">
    	<input type="hidden" name="comment_id" value="" id="comment_id">
    	<input type="hidden" name="published_at" value="{{ now() }}">
    	  @guest
    	   <input type="hidden" value="null" name="user_id" id="user_id">
    	   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 error">
                <input type="text" class="form-control" id="name" placeholder="Ingresa tu nombre" name="author_name">
            </div>
            
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <input type="email" class="form-control" id="email" placeholder="Ingresa tu email" name="author_email">
            </div>
    	  @else 
    	   <input type="hidden" value="{{ auth()->user()->id }}" name="user_id">

    	   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 error">
                <input type="text" class="form-control" id="author_name" placeholder="{{ auth()->user()->name }}" name="author_name" value="{{ auth()->user()->name }}">
            </div>
            
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <input type="email" class="form-control" id="author_email" placeholder="{{ auth()->user()->email }}" name="author_email" value="{{ auth()->user()->email }}">
            </div>
    	  @endguest
        
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	           <input type="text" class="form-control" id="website" placeholder="Ingresa la Url" name="author_url">
	        </div>

	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	          <textarea class="form-control" id="txtBody" placeholder="Escribe tu comentario" rows="4" name="body"></textarea>
            </div>

           </div>
        <div class="row">
            <div class="col-md-3" id="submit-form-button">
                <button id="submit-comment" type="submit" class="comment-submit btn btn-blue">Enviar comentario</button>
            </div>
        </div>
    </form>
</div> <!--/.comment_form_box -->

@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			$('#submit-comment').on('click', function(e){
				e.preventDefault();
				var idForm = 'form-comments';
				var form = new FormData(document.getElementById(idForm));
				var action = $('#' + idForm).attr('action');
				formAjax(action, form);
			});

			$('a#comment-reply').on('click', function(e){
				var commentId = $(this).attr('aria-comment-id');
				$('input#comment_id').attr('value', commentId);
				$('#comments-title').text('Respondiendo al comentario de ' + $(this).attr('aria-commenter-name'));
				$('textarea#txtBody').focus();
			});
	
			$('a#comment-reply-response').on('click', function(e){
				var commentId = $(this).attr('aria-comment-id');
				$('input#comment_id').attr('value', commentId);
				$('#comments-title').text('Respondiendo al comentario de ' + $(this).attr('aria-commenter-name'));
				$('textarea#txtBody').focus();
			});			

		});//Document.ready


		function formAjax(action, form){
			var position = $('#message').offset().top;

		    $.ajax({
		    	url: action,
		        type: 'POST',
		        data: form,
		        contentType: false,
		        processData: false,
		        cache: false,
		        beforeSend: function(){
		        	$('#submit-comment').after('<p><img class=id="ajax-loader" src="/theme/img/ajax-loader.gif" id="ajax-loader"></p>');
		        }
		    }).done(function(r){
		    	$('img#ajax-loader').slideUp('slow');
		        $('html, body').animate({
	            scrollTop: position - 80
	            }, 2000);
	            $('#message').slideDown('slow');
	            if(r !== 'true'){
	            	$('#message').html('<p class="alert alert-danger">' + r + '</p>');
	            } else {
	            	$('#message').html('<p class="alert alert-success">Comentario enviado con éxito</p>');
		            setInterval(function(){
		            	location.reload();
		            }, 2000);
		        }
		    });
		}
	</script>
@endpush