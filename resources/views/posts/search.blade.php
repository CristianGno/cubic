@extends('layouts.layout')

@section('content')
<div class="search-head header19 parallax">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1><span>{{ $count }}</span> Resultado{{ ($count > 1) ? 's' : ''}} para  "<span>{{ $keyword }}</span>"</h1>
                    <p>Si no has conseguido resultados, puedes intentar con una nueva búsqueda.</p>
                    <form action="{{ route('search.show') }}" class="search-page-form" method="POST">
                    	{{ csrf_field() }}
                        <input type="text" placeholder="Buscar..." class="search-page-input" required name="keyword" value="{{ isset($keyword) ? $keyword : '' }}">
                        <input type="submit" value="Buscar" class="search-page-sub">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 

<section class="search-results">

  <div class="container">
	@if ($count > 0)
		@foreach ($posts as $post)
			<article class="post-entry clearfix">
				@if ($post->photos->count() > 0)
					<figure class="search-item-media">
						<img src="{{ url($post->photos->first()->url) }}" alt="post thumb" class="img-responsive">
					</figure>
				@endif
					<div class="post-excerpt">
			            <h2><a href{{ route('blog.single', $post) }}">{{ $post->title }}</a></h2>
						@include('posts.partials.posts-meta')
			            <p>{!! $post->excerpt !!}</p>
			            <div class="excerpt-btn">
			                <a href="{{ route('blog.single', $post) }}">Leer más</a>
			            </div>
			        </div>
				</article>
		@endforeach
 
 	  @else

	    <h4>No hay resultados...</h4>

	@endif
	
 </div>
</section>
@endsection