@extends('layouts.layout')

@section('content')
<div class="content-holder">
	<div class="container">
		<div class="row">
          <div class="col-md-8">
            <!-- Post Holder -->
            <article class="entry-post">
                <!-- Post Media -->
               @if ($post->photos->count())
                 <div class="post-media">
                    <img src="{{ $post->photos->first()->url }}" alt="post thumb" class="img-responsive">
                 </div>
               @endif
                <!-- // Post Media -->

                <div class="entry-content">
                    <!-- Post Header -->
                    <header class="post-header">    
                        <h2 class="post-title">
                            {{ $post->title }}
                        </h2>
                        
                    </header>
                    <!-- // Post Header -->

                    <!-- Post Meta -->
                    @include('posts.partials.posts-meta')
                    <!-- // Post Meta -->

                    <!-- Post Excerpt -->
                    <div class="entry-body">
                        {!! $post->body !!}
                    </div>
                    <!-- // Post Excerpt -->

                    <!-- Social Sharer -->
                    @include('posts.partials.social-sharer')
                    <!-- // Social Sharer -->
                </div>
            </article>
            <!-- // Post Holder -->

            <nav class="next-prev-post clearfix">
                <a href="#" class="pull-left" style="color:white;">Previous Post</a>
                <a href="#" class="pull-right" style="color:white;">Next Post</a>
            </nav>
            @include('posts.partials.author')
            <!-- .post_comment_area -->
              @include('posts.partials.comments')
            <!-- // post_comment_area -->
        </div> <!--.col-md-8-->
        @include('layouts.sidebar')

	  </div>
	</div>
</div>
@endsection