@if ($paginator->hasPages())
<nav class="post-pagination">
    <ul>
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span></span></li>
        @else
            <li>
                <a href="{{ $paginator->previousPageUrl() }}" >
                    <span class="arrow">&#8592;</span>
                    <span class="prev">Prev</span>
                </a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled">{{ $element }}</li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active">{{ $page }}</li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
               <a href="{{ $paginator->nextPageUrl() }}" >
                  <span class="arrow">&#8594;</span>
                  <span class="next">Next</span>
               </a>
            </li>
        @else
            <li class="disabled"><span></span></li>
        @endif
    </ul>
</nav>
@endif
