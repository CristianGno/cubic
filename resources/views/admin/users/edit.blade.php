@extends('admin.layouts.layout')  

@section('content') 
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					Editar usuario {{ $user->name }}
				</h3>				
			</div>

			<div class="box-body">
			  @if ($errors->any())
			  	<ul class="list-group">
			  		@foreach ($errors->all() as $error)
			  			<li class="list-group-item list-group-item-danger">
			  				{{ $error }}
			  			</li>
			  		@endforeach
			  	</ul>
			  @endif
				@if ($picture = $user->picture)
				 <div class="row">
				    <div class="pull-left image col-xs-6">
				      <img src="{{ url($user->picture->first()->url) }}"
				       class="img-circle img-responsive" 
				       alt="Imagen de perfil">
				    </div>					
				</div>
				@endif


				<form method="POST" action="{{ route('admin.users.update', $user) }}" enctype="multipart/form-data" >
					{{ csrf_field() }} {{ method_field('PUT') }}

					<div class="form-group">
						<label for="picture">Imagen de perfil</label>
						<input type="file" name="picture" class="form-control">
					</div>
					
					<div class="form-group">
					  <label for="name">Nombre:</label>
						<input type="text" name="name" value="{{ old('name', $user->name) }}" class="form-control">
					</div>

					<div class="form-group">
						<label for="email">Email:</label>
						<input type="email" name="email" value="{{ old('email', $user->email) }}" class="form-control">
					</div>

					<div class="form-group">
						<label for="">Biografía:</label>
						<textarea name="biography" rows="5" class="form-control" >{{ old('biography', $user->biography) }}</textarea>
					</div>

					<div class="form-group">
						<label for="">Facebook:</label>
						<input type="text" class="form-control" name="facebook" value="{{ old('facebook', $user->facebook) }}">
					</div>

					<div class="form-group">
						<label for="">Twitter:</label>
						<input type="text" class="form-control" name="twitter" value="{{ old('twitter', $user->twitter) }}">
					</div>

					<div class="form-group">
						<label for="">Github:</label>
						<input type="text" class="form-control" name="github" value="{{ old('github', $user->github) }}">
					</div>

					<div class="form-group">
						<label for="">Google + </label>
						<input type="text" class="form-control" name="google_plus" value="{{ old('google_plus', $user->google_plus) }}">
					</div>


					<div class="form-group">
						<label for="password">Contraseña:</label>
						<input type="password" name="password" class="form-control" placeholder="Contraseña">
						<span class="help-block">
							Dejar en blanco si no quieres cambiar la contraseña.
						</span>
					</div>

					<div class="form-group">
						<label for="password_confirmation">Repite la contraseña</label>
						<input type="password" name="password_confirmation" class="form-control" placeholder="Repite la contraseña">
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary form-control">
							Actualizar usuario
						</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title with-border">
					Roles
				</h3>
			</div>

			  <div class="box-body">
				@role('Admin')

				 <form method="POST" action="{{ route('admin.users.roles.update', $user) }}">
				  	{{ csrf_field() }} {{ method_field('PUT') }}

						<div class="row">
							@include('admin.roles.checkboxes')
						</div>

					 <div class="form-group">
					 	<button class="bnt btn-primary form-control">
					 		Actualizar roles
					 	</button>
					 </div>
				  </form>

				  @else

				  	<ul class="list-group">
				  		@forelse($user->roles as $role)
							<li class="list-group-item"> - {{ $role->name }}</li>
				  		@empty
				  		<li class="list-group-item">No tienes ningún rol</li>
				  		@endforelse
				  	</ul>					

				@endrole
				</div>
		     </div>

		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title with-border">
					Permisos
				</h3>
			</div>

			  <div class="box-body">
				 @role('Admin')
				 <form method="POST" action="{{ route('admin.users.permissions.update', $user) }}">
				  	{{ csrf_field() }} {{ method_field('PUT') }}
					 
					  <div class="row">
					  	@include('admin.permissions.checkboxes')
					  </div>

					 <div class="form-group">
					 	<button class="bnt btn-primary form-control">
					 		Actualizar permisos
					 	</button>
					 </div>
				  </form>
				  @else
				  	<ul class="list-group">
				  		@forelse($user->permissions as $permission)
				  			<li class="list-group-item">- {{ $permission->name }}</li>
				  		@empty
				  			<li class="list-group-item">No tienes ningún permiso</li>
				  		@endforelse
				  	</ul>
				  @endrole
				</div>
		     </div>

	      </div><!--/.Roles-->
       </div><!--/.row-->

@endsection
