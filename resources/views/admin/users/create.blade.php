@extends('admin.layouts.layout')

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-primary" style="padding:20px;">
				<div class="box-header">
					<h3 class="box-header with-border" >Crear usuario</h3>
				</div>

				<div class="box-body">
					@if ($errors->any())
						<ul class="list-group">
							@foreach ($errors->all() as $error)
								<li class="list-group-item list-group-item-danger">
									{{ $error }}
								</li>
							@endforeach
						</ul>
					@endif
					<form method="POST" action="{{ route('admin.users.store') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="name">Nombre:</label>
							<input type="text" class="form-control" name="name" value="{{ old('name') }}">
						</div>

						<div class="form-group"><label for="email">Email</label>
							<input type="text" class="form-control" name="email" value="{{ old('email') }}">
						</div>

						<div class="form-group col-md-6">
							<label for="">Roles</label>
							  @include('admin.roles.checkboxes')	
						</div>

						<div class="form-group col-md-6">
							<label for="">Permisos</label>
							  @include('admin.permissions.checkboxes')
						</div>

						<span class="help-block">
							La contraseña será generada y enviada al usuario vía email
						</span>

					    <div class="form-group">
							<button type="submit" class="btn btn-primary form-control">
								Crear usuario
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
