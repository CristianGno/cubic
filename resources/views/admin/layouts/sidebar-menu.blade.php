  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          @if ($pic = auth()->user()->picture)
            <img src="{{ url($pic->url) }}" class="img-circle" alt="User Image">
            <br><br>
          @else 
           <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{ auth()->user()->name }}</p>
          <!-- Status -->

          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"></li>
        <li>
          <a href="{{ route('home') }}" target="_blank">Visitar sitio</a>
        </li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{{ request()->is('admin') ? 'active' : '' }}">
          <a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> <span>Inicio</span></a>
        </li>

        <li class="treeview {{ request()->is('admin/posts/*') ? 'active' : '' }}">
          <a href="#"><i class="fa fa-bars"></i> <span>Publicaciones</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ request()->is('admin/posts') ? 'active' : '' }}">
              <a href="{{ route('admin.posts.index') }}">
                <i class="fa fa-eye"></i>
                Ver todas las publicaciones
              </a>
            </li>

            <li class="{{ request()->is('admin/posts/create') ? 'active' : '' }}">
              <a href="{{ route('admin.posts.create') }}">
                <i class="fa fa-plus"></i>
                Crear publicación
              </a>
            </li>
            
            <li class="{{ request()->is('admin.categories./*') ? 'active' : '' }}">
              <a href="{{ route('admin.categories.index') }}">
                <i class="fa fa-archive"></i>
                Categorías
              </a>
            </li>

            <li class="{{ request()->is('admin.tags.*') ? 'active' : '' }}">
              <a href="{{ route('admin.tags.index') }}">
                <i class="fa fa-tag"></i>
                Etiquetas
              </a>
            </li>

          </ul>
        </li>
      
      @role('Admin')

      <li class="treeview {{ request()->is('admin/pages/*') ? 'active' : '' }}">
          <a href="#"><i class="fa fa-file"></i> <span>Páginas</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu {{ request()->is('admin/pages') ? 'active' : '' }}">
            <li class="{{ request()->is('admin/pages') ? 'active' : '' }}">
              <a href="{{ route('admin.pages.index') }}">
                <i class="fa fa-eye"></i>
                Ver todas las páginas
              </a>
            </li>

            <li class="{{ request()->is('admin/pages/create') ? 'active' : '' }}">
              <a href="{{ route('admin.pages.create') }}">
                <i class="fa fa-plus"></i>
                Crear página
              </a>
            </li>

          </ul>
      </li>
  

        <li class="treeview {{ request()->is('admin/users/*') ? 'active' : '' }}">
          <a href="#"><i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu {{ request()->is('admin/users') ? 'active' : '' }}">
            <li class="{{ request()->is('admin/users') ? 'active' : '' }}">
              <a href="{{ route('admin.users.index') }}">
                <i class="fa fa-eye"></i>
                Ver todos los usuarios
              </a>
            </li>

            <li class="{{ request()->is('admin/users/create') ? 'active' : '' }}">
              <a href="{{ route('admin.users.create') }}">
                <i class="fa fa-plus"></i>
                Crear Usuario
              </a>
            </li>

          </ul>
        </li>


        <li class="treeview">
          <a href="#"><i class="fa fa-comments"></i> <span>Comentarios</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu">
            <li class="">
              <a href="{{ route('admin.comments.index') }}">
                <i class="fa fa-comment"></i>
              Ver todos los comentarios
              </a>
            </li>
          </ul>
        </li>
      @else
      <li>
        <a href="{{ route('admin.users.edit', auth()->user()->id) }}">Perfil</a>
      </li>
      @endrole

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>