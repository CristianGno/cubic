@include('admin.layouts.header')
  <!-- Left side column. contains the logo and sidebar -->
@include('admin.layouts.sidebar-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content-header">
     
     @php
       levels();
     @endphp
     
    </section>
<br><br>

@if (session()->has('flash'))
  <section class="content-header">
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="color:white">X</button>
      <h5>
         <i class="fa fa-check" style="color:white"></i>
          {{ session('flash') }}
        
      </h5>
    </div>
  </section>
@endif

    <!-- Main content -->
    <section class="content container-fluid">

      
        <!--| Your Page Content Here | -->
          
          @yield('content')
        
        <!--| end Content| -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
 @include('admin.layouts.footer')