@extends('admin.layouts.layout')

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">
					Editando el comentario
				</h3>
			</div>

			<div class="box-body">
				@if ($errors->any())
					<ul class="list-group">
						@foreach ($errors->all() as $error)
							<li class="list-group-item list-group-item-danger">
								{{ $error }}
							</li>
						@endforeach
					</ul>
				@endif

				@php
					$post = $comment->post;
				@endphp
				
				<form method="POST" action="{{ route('admin.comments.update', $comment) }}">
					{{ csrf_field() }} {{ method_field('PATCH') }}
					<div class="form-group {{ $errors->has('author_name') ? 'has-error' : '' }}">
						<label for="">Autor:</label>
						<input type="text" name="author_name" value="{{ old('author_name', $comment->author_name) }}" class="form-control">
					</div>

					<div class="form-group {{ $errors->has('author_email') ? 'has-error' : '' }}">
						<label for="">Email:</label>
						<input type="email" name="author_email" value="{{ old('author_email', $comment->author_email) }}" class="form-control">
					</div>

					<div class="form-group {{ $errors->has('author_url') ? 'has-error' : '' }}">
						<label for="">Url:</label>
						<input type="text" name="author_url" value="{{ old('author_url', $comment->author_url) }}" class="form-control">
					</div>

					<div class="form-group {{ $errors->has('author_name') ? 'has-error' : '' }}">
						<label for="">Comentario:</label>
						<textarea name="body" class="form-control" rows="10">{!! old('body', $comment->body) !!}</textarea>
					</div>

					<div class="form-group">
						<button class="btn btn-primary form-control">actualizar comentario</button>
					</div>
				</form>

				@if($comment->approved == 0)
					<form method="POST" action="{{ route('admin.comments.approve', $comment) }}">
						{{ csrf_field() }} {{ method_field('PATCH') }}

						<div class="form-group">
							<inp value="1">
							<button class="btn btn-success form-control">
								Aprobar
							</button>
						</div>
					</form>
				@else
					<form method="POST" action="{{ route('admin.comments.disapprove', $comment) }}">
						{{ csrf_field() }} {{ method_field('PATCH') }}

						<div class="form-group">

							<button class="btn btn-danger form-control">
								Desaprobar
							</button>
						</div>
					</form>
				@endif
			</div>
		</div>
	</div>

	@if ($comment->responses->count() > 0)
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Respuestas a este comentario</h3>
				</div>

				<div class="box-body">
				   <ul class="list-group">
					  @foreach ($comment->responses as $response)
						<li class="list-group-item">
							<a href="">{{ $response->author_name }}</a>

							<div class="pull-right">
								<a target="_blank" href="{{ route('blog.single', $post) . '#comment-' .$response->id  }}" class="btn btn-primary btn-xs" title="Ver">
									<i class="fa fa-eye"></i>
								</a>

								@if($response->approved == 0)
									<form method="POST" action="{{ route('admin.responses.approve', $response) }}" style="display: inline-block;">
										{{ csrf_field() }} {{ method_field('PATCH') }}			
										<button class="btn btn-success btn-xs" title="aprobar">
											<i class="fa fa-check"></i>
										</button>
									</form>
								@else
									<form method="POST" action="{{ route('admin.responses.disapprove', $response) }}" style="display: inline-block;">
										{{ csrf_field() }} {{ method_field('PATCH') }}
			
										<button class="btn btn-danger btn-xs" title="desaprobar">
											<i class="fa fa-ban"></i>
										</button>
									</form>
								@endif

								<form action="{{ route('admin.responses.destroy', $response) }}" method="POST" style="display: inline-block;">
									{{ csrf_field() }} {{ method_field('DELETE') }}
									<button onclick="return confirm('¿deseas eliminar este comentario?')" class="btn btn-danger btn-danger btn-xs" title="Eliminar">
										<i class="fa fa-remove"></i>
									</button>
								</form>								
							</div>
						</li>	
					  @endforeach
					</ul>  
				</div>
			</div>
		</div>
	@endif
</div>
@endsection