@extends('admin.layouts.layout')  

@section('content')   
<div class="box box-primary">
{{--     <div class="box-header">
      <h3 class="box-title">Permisos</h3>
      <a class="btn btn-primary pull-right" href="{{ route('admin.comments.create') }}">
        <i class="fa fa-plus"></i> Crear Permiso
      </a>
    </div> --}}
    <!-- /.box-header -->
    <div class="box-body">
      <table id="comments-table" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Author</th>
          <th>Post</th>
          <th>Acciones</th>
        </tr>
        </thead>
		
		<tbody>
			@foreach ($comments as $comment)
        @php
          $post = $comment->post;
        @endphp
				<tr>
					<td>{{ $comment->id }}</td>
					<td>{{ $comment->author_name }}</td>
          <td>
            <a href="{{ route('blog.single', $post) }}">
              {{ $post['title'] }}
            </a>
          </td>
					<td>
            @if (!is_null($comment->approved))
              <a target="_blank" href="{{ route('blog.single', $post) . '#comment-' . $comment->id }}"
                class="btn btn-default btn-xs">
                <i class="fa fa-eye"></i>
              </a>
            @endif

						<a href="{{ route('admin.comments.edit', $comment) }}" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>

            <form action="{{ route('admin.comments.destroy', $comment) }}" style="display: inline" method="POST">
              {{ csrf_field() }} {{ method_field('DELETE') }}

              <button  class="btn btn-xs btn-danger"
                onclick="return confirm('¿Estás seguro que deseas eliminar esta entrada?');">
              <i class="fa fa-times"></i>
            </button>
            </form>
					</td>
				</tr>
			@endforeach
		</tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

  @push('styles')
  	<link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  @endpush

  @push('scripts')
  
  	<script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

     <script>
	  $(function () {
	    $('#comments-table').DataTable({
	      'paging'      : true,
	      'lengthChange': true,
	      'searching'   : true,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	  })
	</script>
  @endpush

@endsection
