@extends('admin.layouts.layout')

@section('content')
<form action="{{ route('admin.posts.store') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="row">
		<div class="col-md-8">
			<div class="box box-primary">
				<div class="box-header">
					<h1 class="box-title">Crear publicación</h1>
				</div>
				  <div class="box-body ">
					<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title">Título de la publicación</label>
						<input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Ingresa el título de la publicación">
						<span class="help-block">
						  {{ $errors->first('title') }}
					    </span>
					</div>

					<div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
						<label for="">Contenido de la publicación</label>
						<div class="row">
							<div class="col-xs-12" style="margin: 10px;">
								<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-images">
                			       Agregar imagen(es) <i class="fa fa-image"></i>
             			       </button>
							</div>
						</div>
						<textarea name="body" id="editor" class="form-control" placeholder="Ingresa el contenido completo de la publicación">{{ old('body') }}</textarea>
						<span class="help-block">
						  {{ $errors->first('body') }}
					    </span>
					</div>
				  </div>
			</div>
		</div> <!--/.col-md-8 -->

		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header">
					<h1 class="box-title">
						
					</h1>
				</div>

				<div class="box-body">
		             <div class="form-group {{ $errors->has('published_at') ? 'has-error' : '' }}">
		                <label>Fecha de publicación:</label>

			                <div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input type="text" name="published_at" class="form-control pull-right" id="datepicker" value="{{ old('published_at') }}">
			                  <span class="help-block">
						       {{ $errors->first('published_at') }}
					          </span>
			                </div>
		                <!-- /.input group -->
		              </div>

		              <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
		              	<label for="category_id">Categoría</label>
		              	<select name="category_id" class="form-control select2">
		              		<option value="">Selecciona una categoría</option>
		              		@foreach ($categories as $category)
		              			<option value="{{ $category->id }}"
		              					{{ old('category_id') == $category->id ? 'selected' : '' }}>
		              				{{ $category->name }}
		              			</option>
		              		@endforeach
		              	</select>
		              	<span class="help-block">
						  {{ $errors->first('category_id') }}
					    </span>
		              </div>	
						
					  <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
					  	<label for="tags">
					  		Etiquetas
					  	</label>

					  	<select name="tags[]" class="form-control select2" multiple="multiple" data-placeholder="Selecciona las etiquetas para la publicación">
					  		@foreach ($tags as $tag)
					  			<option value="{{ $tag->id }}" 
					  					{{ collect(old('tags'))->contains($tag->id) ? 'selected' : '' }}>
					  				{{ $tag->name }}
					  			</option>
					  		@endforeach
					  	</select>
					  	<span class="help-block">
						  {{ $errors->first('tags') }}
					    </span>
					  </div>

		              <div class="form-group {{ $errors->has('excerpt') ? 'has-error' : '' }}">
						<label for="excerpt">
							Extracto de la publicación
						</label>
						<textarea type="text" name="excerpt" class="form-control" placeholder="Extracto de la publicación" value="{{ old('excerpt') }}"></textarea>
						<span class="help-block">
						  {{ $errors->first('excerpt') }}
					    </span>
					</div>

					<div class="form-group {{ $errors->has('thumbnail') ? 'has-error' : '' }}">
						<label for="thumbnail">Imagen destacada de la publicación</label>
						<input type="file" class="form-control" name="thumbnail">
						<span class="help-block">
							{{ $errors->first('thumbnail') }}
						</span>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							Guardar publicación
						</button>
					</div>


				</div>
			</div>
		</div><!--/.col-md-4 -->
	</div>
</form>

@push('styles')
	<link rel="stylesheet" href="/adminlte/dist/css/dropzone-5.1.0.min.css">
	<link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@endpush

@push('scripts')
	<script type="text/javascript" src="/adminlte/dist/js/dropzone-5.1.0.min.js"></script>
	<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>
	<script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
	
<script type="text/javascript">
$('#datepicker').datepicker({
  autoclose: true
});
    CKEDITOR.replace('editor');
    CKEDITOR.config.height = 317;

    Dropzone.autoDiscover = false;

//Initialize Select2 Elements
$('.select2').select2({
	'tags': true,
});
</script>
@endpush

@include('admin.posts.partials.modal-images')
@endsection