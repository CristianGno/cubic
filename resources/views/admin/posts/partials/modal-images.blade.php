<div class="modal" id="modal-images">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Biblioteca de imágenes</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Desde la biblioteca</a></li>
              <li><a href="#tab_2" data-toggle="tab">Subir imagen</a></li>
              <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                Biblioteca de imágenes
                <div class="row" style="padding: 19px;" id="imagesServer">
                    @foreach ($images as $image)
                      <div class="col-xs-3">
                        <img src="{{ url($image->url) }}" class="img-responsive" style="width: 100px; height: 100px; padding: 6px;">
                      </div>
                    @endforeach
                </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">

                <p>Subir imágenes</p>
                <br>
                <br>
                <div class="dropzone">
                  
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                like Aldus PageMaker including versions of Lorem Ipsum.
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
     </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
        <!-- /.modal -->

@push('scripts')
<script>
  $('#modal-images').modal({
    'backdrop': false,
    'show': false,
  });

   var myDropzone = new Dropzone('.dropzone', {
      url: "{{ route('admin.photos.store') }}",
      acceptedFiles: 'image/*',
      paramName: 'images',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      dictDefaultMessage: "Arrastra o selecciona las imágenes",
    });

    myDropzone.on('error', function(file, res){
      var msg = res.errors.images[0];
      $('.dz-error-message:last > span').text(msg);
    });

    myDropzone.on('success', function(file, res){
      var html = '<div class="col-xs-3"> <img src="' + res + '" class="img-responsive" style="width: 100px; height: 100px; padding: 6px;"> </div>';
      var img = '<img src=" '+ res +' " >';
      var editor = $('#editor');
      alert(editor[0].val());
      $('#imagesServer').append(html);

    });

    Dropzone.autoDiscover = false;
  
</script>
@endpush