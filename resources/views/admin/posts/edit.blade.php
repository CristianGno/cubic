@extends('admin.layouts.layout')

@section('content')
	<div class="row">
		@if($photo = $post->photos->count() > 0)
		 <div class="col-md-8">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			<h3 class="box-title">
		 				Thumbnail de la publicación
		 			</h3>
		 		</div>

		 		<div class="box-body">
		 			<form method="POST" action="{{ route('admin.photos.destroy', $post->photos->first()) }}">
		 				{{ csrf_field() }} {{ method_field('DELETE') }}

		 				<div class="form-group col-md-4">
		 					<button onclick="return confirm('¿Deseas eliminar esta foto?')" type="submit" class="btn btn-danger" style="position: absolute;">
		 						<i class="fa fa-remove"></i>
		 					</button>
		 					<img class="img-responsive" src="{{ url($post->photos->first()->url) }}" alt="">
		 				</div>
		 			</form>
		 		</div>
		 	</div>
		 </div>
		@endif
		<div class="col-md-8">
			<div class="box box-primary">
				<div class="box-header">
					<h1 class="box-title">
						Editar publicación
						<small>
							<a href="{{ route('blog.single', $post) }}" target="_blank">
								Ver publicación
							</a>
						</small>
				    </h1>
				</div>
				 <div class="box-body ">
				 <form action="{{ route('admin.posts.update', $post) }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }} {{ method_field('PUT') }}
					<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title">Título de la publicación</label>
						<input type="text" class="form-control" name="title" value="{{ old('title', $post->title) }}" placeholder="Ingresa el título de la publicación">
						<span class="help-block">
						  {{ $errors->first('title') }}
					    </span>
					</div>

					<div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
						<label for="">Contenido de la publicación</label>
						<textarea name="body" id="editor" class="form-control" placeholder="Ingresa el contenido completo de la publicación">
							{{ old('body', $post->body) }}
						</textarea>
						<span class="help-block">
						  {{ $errors->first('body') }}
					    </span>
					</div>
				  </div>
			</div>
		</div> <!--/.col-md-8 -->

		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header">
					<h1 class="box-title">
						
					</h1>
				</div>

				<div class="box-body">
		             <div class="form-group {{ $errors->has('published_at') ? 'has-error' : '' }}">
		                <label>Fecha de publicación:</label>

			                <div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input type="text" name="published_at" class="form-control pull-right" id="datepicker" value="{{ old('published_at', $post->published_at ? $post->published_at->format('m/d/Y') : '')}}">
			                  <span class="help-block">
						       {{ $errors->first('published_at') }}
					          </span>
			                </div>
		                <!-- /.input group -->
		              </div>

		              <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
		              	<label for="category_id">Categoría</label>
		              	<select name="category_id" class="form-control select2">
		              		<option value="">Selecciona una categoría</option>
		              		@foreach ($categories as $category)
		              			<option value="{{ $category->id }}"
		              					{{ old('category_id', $post->category->id) == $category->id ? 'selected' : '' }}>
		              				{{ $category->name }}
		              			</option>
		              		@endforeach
		              	</select>
		              	<span class="help-block">
						  {{ $errors->first('category_id') }}
					    </span>
		              </div>
						
					  <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
					  	<label for="tags[]">
					  		Etiquetas
					  	</label>

					  	<select name="tags[]" class="form-control select2" multiple="multiple" data-placeholder="Selecciona las etiquetas para la publicación">
					  		@foreach ($tags as $tag)
					  			<option value="{{ $tag->id }}" 
					  					{{ collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : '' }}>
					  				{{ $tag->name }}
					  			</option>
					  		@endforeach
					  	</select>
					  	<span class="help-block">
						  {{ $errors->first('tags') }}
					    </span>
					  </div>

		              <div class="form-group {{ $errors->has('excerpt') ? 'has-error' : '' }}">
						<label for="excerpt">
							Extracto de la publicación
						</label>
						<textarea type="text" name="excerpt" class="form-control" placeholder="Extracto de la publicación" value="">{{ old('excerpt', $post->excerpt) }}</textarea>
						<span class="help-block">
						  {{ $errors->first('excerpt') }}
					    </span>
					</div>
					@if ($photo = $post->photos->count() === 0)
					  <div class="form-group {{ $errors->has('thumbnail') ? 'has-error' : '' }}">
						<label for="thumbnail">Imagen destacada de la publicación</label>
						<input type="file" class="form-control" name="thumbnail">
						<span class="help-block">
							{{ $errors->first('thumbnail') }}
						</span>
					</div>
				    @endif 

						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">
								Actualizar publicación
							</button>
						</div>
					</div>
				</div>
			</div><!--/.col-md-4 -->
		</div>
			</form>

@push('styles')
	<link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@endpush

@push('scripts')
	<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>
	<script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
	
<script type="text/javascript">
$('#datepicker').datepicker({
  autoclose: true
});
    CKEDITOR.replace('editor');
    CKEDITOR.config.height = 317;


//Initialize Select2 Elements
$('.select2').select2({
	'tags': true,
});
</script>
@endpush

@if ($post->photos->count() === 0)
	@push('scripts')
	<script type="text/javascript" src="/adminlte/dist/js/dropzone-5.1.0.min.js"></script>

		{{-- <script>
		   //Dropzone para imágenes
		   Dropzone.autoDiscover = false;

		    var myDropzone = new Dropzone('.dropzone', {
		    	url: '/admin/posts/{{ $post->url }}/photos',
		    	acceptedFiles: 'image/*',
		    	maxFilesize: 2,
		    	paramName: 'photo',
		    	maxFiles: 1,
		    	headers: {
		    		'X-CSRF-TOKEN': '{{ csrf_token() }}'
		    	},
		    	dictDefaultMessage: "Arrastra aquí la foto del thumbnail",
		    });

		    myDropzone.on('error', function(file, res){
		    	var msg = res.errors.photo[0];
		    	$('.dz-error-message:last > span').text(msg);
		    });
		</script> --}}
	@endpush

	@push('styles')
	<link rel="stylesheet" href="/adminlte/dist/css/dropzone-5.1.0.min.css">
	@endpush
@endif
@endsection

