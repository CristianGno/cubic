@extends('admin.layouts.layout')

@section('content')
<form action="{{ route('admin.pages.store') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h1 class="box-title">Crear Página</h1>
				</div>
				  <div class="box-body ">
					<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title">Título de la Página</label>
						<input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Ingresa el título de la Página">
						<span class="help-block">
						  {{ $errors->first('title') }}
					    </span>
					</div>

					<div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
						<label for="">Contenido de la Página</label>
						<textarea name="body" id="editor" class="form-control" placeholder="Ingresa el contenido completo de la Página">{{ old('body') }}</textarea>
						<span class="help-block">
						  {{ $errors->first('body') }}
					    </span>
					</div>

					<div class="form-group">
						<button class="btn btn-primary form-control">Crear página</button>
					</div>
				  </div>
			</div>
		</div> <!--/.col-md-8 -->

	</div>
</form>

@push('scripts')
	<script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

    CKEDITOR.replace('editor');
    CKEDITOR.config.height = 317;
    
</script>
@endpush

@endsection