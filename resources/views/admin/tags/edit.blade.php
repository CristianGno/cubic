@extends('admin.layouts.layout')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					Actualizar Etiqueta {{ $tag->name }}
				</h3>
			</div>

			<div class="box-body">
				<form method="POST" action="{{ route('admin.tags.update', $tag) }}">
					{{ csrf_field() }} {{ method_field('PUT') }}
					
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
						<label for="name">Nombre:</label>
						<input type="text" name="name" class="form-control" value="{{ old('name', $tag->name) }}">

						<span class="help-block">{{ $errors->first('name') }}</span>
						
					</div>

					<div class="form-group">
						<button class="btn btn-primary btn-flat form-control">
							Actualizar Categoría
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection
