@extends('admin.layouts.layout')  

@section('content')   
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Crear Permiso</h3>
				</div>

				<div class="box-body">
					<form action="{{ route('admin.permissions.store') }}" method="POST">
						{{ csrf_field() }}

						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<input type="text" name="name" class="form-control" value="{{ old('name') }}">
							<span class="help-block">
								{{ $errors->first('name') }}
							</span>
						</div>

						<div class="form-group">
							<button class="btn btn-primary form-control">
								Crear permiso
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
