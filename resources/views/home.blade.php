@extends('layouts.layout')
@section('content')
 <section id="blog-section">
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8">
          <div class="posts-section">

            @foreach ($posts as $post)
                <article class="post-entry">
                   @if ($post->photos->count())
                     <div class="post-media">
                        <img src="{{ $post->photos->first()->url }}" alt="post thumb" class="img-responsive">
                     </div>
                   @endif
                    <div class="post-excerpt">
                        <h2><a href="{{ route('blog.single', $post) }}">{{ $post->title }}</a></h2>
                          @include('posts.partials.posts-meta')
                            <p>
                                {{ $post->excerpt }}
                            </p>
                        <div class="excerpt-btn">
                            <a href="{{ route('blog.single', $post) }}">Leer más</a>
                        </div>
                        <ul class="post-share text-right">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div>
                </article>
                <!-- end .post-entry -->
             @endforeach
              {{ $posts->links() }}


            </div>
        </div> <!-- end .col-xs-12 col-md-8 -->

                    <!-- SIDEBAR -->
@include('layouts.sidebar')

            </div>
        </div>
    </section>
    <!--
    End #blog-section
@endsection
       