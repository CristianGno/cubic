    <div class="col-xs-12 col-md-4">
        <div class="right-sidebar">

            <aside class="sidebar">
                <form action="{{ route('search.show') }}" method="post" class="blog-search">
                    {{ csrf_field() }}
                    <input type="text" name="keyword" class="search-input" placeholder="Buscar aquí">
                    <button type="submit" class="search-sub">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </aside>
            <!-- end .sidebar -->

        @if ($categories->count() > 0)
            <aside class="sidebar">
                <h4 class="widget-title">Categorías</h4>
                <ul>
                  @foreach ($categories as $category)
                    <li>
                      <a href="{{ route('categories.show', $category) }}">
                        <i class="fa fa-angle-right"></i>
                         {{ $category->name }}
                     </a>
                    </li>       
                  @endforeach   
                </ul>
            </aside>
            <!-- end .sidebar -->
        @endif

        @if(isset($populars))
            <aside class="sidebar">
                <h4 class="widget-title">Últimas entradas</h4>
                @foreach ($populars as $popular)
                    <div class="pp-item clearfix">
                      @if ($thumbnail = $popular->photos->first())
                        <a href="#">
                            <img src="{{ url($thumbnail->url) }}">
                        </a>
                      @else 
                        <a href="#">
                            <img src="/theme/images/blog/client4.png" alt="">
                        </a>
                      @endif
                    <div class="pp-media">
                        <span>{{ $popular->published_at->format('d/m/Y') }}</span>
                        <h4><a href="{{ route('blog.single', $popular) }}">{{ $popular->title }}</a></h4>
                    </div>
                </div>
                @endforeach
            </aside>
            <!-- end .sidebar -->
        @endif

          @if ($tags->count() > 0)
            <aside class="sidebar">
                <h4 class="widget-title">Tags</h4>
                <div class="tagcloud">
                    @foreach ($tags as $tag)
                      <a href="{{ route('tags.show', $tag) }}">
                          {{ $tag->name }}
                      </a>
                    @endforeach
                </div>
            </aside>
            <!-- end .sidebar -->
          @endif

        </div>
    </div> <!-- end .col-xs-12 col-md-4 -->