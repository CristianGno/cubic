
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Always force latest IE rendering engine or request Chrome Frame -->
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="utf-8">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Site Title -->
        <title>
          @if (isset($title))
              {{ siteTitle($title) }}
          @else
              {{ siteTitle() }}
          @endif
        </title>
            
        <link rel="shortcut icon" href="/theme/img/icons/favicon.png">
        
        <!--
        Google Fonts
        ============================================= -->
<!-- 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,700italic,300italic">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arvo:400,700">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Dosis:800,700">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Great+Vibes"> -->
        
<!--[CSS]
        ============================================= -->
        <link type="text/css" rel="stylesheet" href="/theme/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/jquery.bxslider.css">
        <link type="text/css" rel="stylesheet" href="/theme/js/vendor/owl.carousel.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/magnific-popup.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/lightbox.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/icomoon.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/flaticon.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/supersized.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/bootstrap-timepicker.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/jquery-ui.min.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/animate.css">
     {{--    <link type="text/css" rel="stylesheet" href="/theme/css/multiscroll.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/selectize.default.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/jquery.fullPage.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/bbpress.css">
        <link type="text/css" rel="stylesheet" href="/theme/css/jquery.timepicker.css">
        <link type="text/css" rel="stylesheet" href="/theme/syntax-highlighter/scripts/prettify.min.css"> --}}
        <!-- Shortcodes main stylesheet -->
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/prettyPhoto.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/eislideshow.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/nivo-slider.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/liteaccordion.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/flexslider.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/iconmoon.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/slicebox.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/camera.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/main.css">
        <link type="text/css" rel="stylesheet" href="/theme/shortcodes/css/media-queries.css">
        <!-- Main Stylesheet -->
        <link type="text/css" rel="stylesheet" href="/theme/css/main.css">
        <!-- CSS media queries -->
        <link type="text/css" rel="stylesheet" href="/theme/css/media-queries.css">
        <link id="themeColorChangeLink" type="text/css" rel="stylesheet" href="/theme/css/colors/c1.css">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>


    <body class="bordered" style="background-color: #e5e6e3;" >

        
        <!-- Preloader Two 
        <div id="preloader">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div> -->
<div id="app">
    <div id="wrapper" class="main-wrapper">
        <!-- 
        Navigation
        ==================================== -->
        <header id="head" class="navbar-default sticky-header">
            <div class="container">
                <div class="mega-menu-wrapper border clearfix">
                    <div class="navbar-header">
                        <!-- responsive nav button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- /responsive nav button -->                    
                        <!-- logo -->
                        <a class="navbar-brand" href="/theme/index.php">
                            <img src="/theme/img/icons/logo.png" alt="Eydia">
                        </a>
                        <!-- /logo -->
                    </div>
                    <!-- main nav -->
                    <nav class="collapse navbar-collapse navbar-right" style="z-index: 10000000;">
                        <ul class="nav navbar-nav">
                            <li class="current"><a href="{{ route('home') }}">Inicio</a></li>
                            <li><a href="#">Acerca de</a></li>
                            <li><a href="#">Archivo</a></li>
                            <li><a href="#">Contacto</a></li>
                            @guest
                              <li><a href="{{ route('login') }}">Login</a></li>
                              <li><a href="{{ route('register') }}">Registro</a></li>
                            @endguest
                        </ul>
                    </nav>
                    <!-- /main nav -->
                </div>                
            </div>
        </header>
        <!--
        End Navigation
        ==================================== -->
        <!--
        #blog-section
        ==================================== -->

        