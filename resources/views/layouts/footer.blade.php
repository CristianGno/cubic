 
    
                <!-- FOOTER -->   <!--
    footer
    ==================================== -->
    <footer class="footer">
        <div class="container">
            <p class="copyright pull-left item_left"> Copyright &copy; 2015 Eydia. All rights reserved. design by <strong><a href="#">pixel studio</a>.</strong></p>
            <ul class="pull-right social-links text-center item_right">
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-behance"></i></a></li>
            </ul>
        </div>
    </footer>
    <!--
    End footer
    ==================================== -->
    
    <!-- back to top -->
    <a href="javascript:;" id="go-top">
        <i class="fa fa-angle-up"></i>
        top
    </a>
    <!-- end #go-top -->
</div>
<!--
    JavaScripts
    ========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="/theme/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="/theme/js/vendor/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="/theme/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.appear.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="/theme/js/vendor/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.backstretch.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.nav.js"></script>
    <script type="text/javascript" src="/theme/js/lightbox.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.parallax-1.1.3.js"></script>
    <!--<script type="text/javascript" src="/theme/http://maps.googleapis.com/maps/api/js?sensor=true"></script>-->
    <script type="text/javascript" src="/theme/js/jquery-validation.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="/theme/js/form.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.multiscroll.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery-countTo.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.mb.YTPlayer.min.js"></script>
    <script type="text/javascript" src="/theme/js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="/theme/js/tweetie.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.nicescroll.min.js"></script>
    <!--
    <script type="text/javascript" src="/theme/js/okvideo.min.js"></script>
    -->
    <script type="text/javascript" src="/theme/js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.easypiechart.js"></script>
    <script type="text/javascript" src="/theme/js/masonry.pkgd.js"></script>
    <!--<script type="text/javascript" src="/theme/js/jquery.tubular.1.0.js"></script>-->
    <script type="text/javascript" src="/theme/js/kenburned.min.js"></script>
    <script type="text/javascript" src="/theme/js/mediaelement-and-player.min.js"></script>
    <!-- shortcode scripts -->
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.slicebox.min.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.eislideshow.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/camera.min.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/jquery.zaccordion.min.js"></script>
    <script type="text/javascript" src="/theme/shortcodes/js/main.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.prettySocial.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.countdown.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.webticker.min.js"></script>
    <script type="text/javascript" src="/theme/js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/theme/js/selectize.min.js"></script>
    <!-- image filter -->
    <script type="text/javascript" src="/theme/js/img-filter/jquery.gray.min.js"></script>
    <!-- // image filter -->
    <script type="text/javascript" src="/theme/js/wow.min.js"></script>
   <!-- <script src="/theme/syntax-highlighter/scripts/prettify.min.js"></script> -->
    <!--<script type="text/javascript">$.SyntaxHighlighter.init();</script>-->
    <script type="text/javascript" src="/theme/js/main.js"></script>
   {{--  <script src="/js/app.js"></script> --}}

    @stack('scripts')


  </div><!--/#app -->
</body>
</html>