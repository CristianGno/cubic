@extends('layouts.layout', ['title' => $page->title])

@section('content')
<div class="content-holder">
	<div class="container">
		<div class="row">
          <div class="col-md-8">
            <!-- Post Holder -->
            <article class="entry-post">
                <!-- Post Media -->
                <!-- // Post Media -->

                <div class="entry-content">
                    <!-- Post Header -->
                    <header class="post-header">    
                        <h2 class="post-title">
                            {{ $page->title }}
                        </h2>
                        
                    </header>

                    <!-- Post Excerpt -->
                    <div class="entry-body">
                        {!! $page->body !!}
                    </div>
                    <!-- // Post Excerpt -->

                    <!-- Social Sharer -->
					@include('posts.partials.social-sharer')
                    <!-- // Social Sharer -->
                </div>
            </article>
            <!-- // Post Holder -->
            <!-- // post_comment_area -->
        </div> <!--.col-md-8-->
        @include('layouts.sidebar')

	  </div>
	</div>
</div>
@endsection