@component('mail::message')
# Hola, {{ $user->name }}

Para acceder a {{ config('name') }} primero debes confirmar tu cuenta. Puedes hacerlo en el siguiente enlace.

@component('mail::button', ['url' => $link])
Confirmar cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
