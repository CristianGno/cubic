@component('mail::message')
# Tus credenciales para {{ config('app.name') }}

Tu cuenta ha sido confirmada, Utiliza estas credenciales para ingresar al sistema.

@component('mail::table')
    | Usuario | Contraseña |
    |:--------|:-----------|
    | {{ $user->email }} | La que escogiste al momento del registro |
@endcomponent

@component('mail::button', ['url' => url('login')])
Iniciar sesión
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
