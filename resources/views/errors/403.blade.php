@extends('admin.layouts.layout')

@section('content')
<div class="container">
	<div class="row text-center">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-danger">
				<div class="box-header">
					<h2>Acción no autorizada</h2>
				</div>
				<div class="box-body">
					<p>
						Esta acción no está autorizada. Por favor vuelve a 
						<a href="{{ route('admin.dashboard') }}">
							atrás
						</a>.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection