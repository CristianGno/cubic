@extends('layouts.layout')

@section('content')
<div class="pages error404">
    <div class="container">
        <div class="error-inner">
            <h2>404!</h2>
            <h4>Opps... Esta página no existe...</h4>
            <p>
            	Por favor regresa al <a href="{{ route('home') }}">Inicio</a> o intenta con una búsqueda.
            </p>
            <form action="{{ route('search.show') }}" method="post" class="blog-search">
                {{ csrf_field() }}
                <input type="text" name="s" class="search-input" placeholder="Buscar">
                <button type="submit" class="search-sub">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>
</div>
@endsection