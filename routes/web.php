<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('email', function(){
	return new App\Mail\LoginCredentials(App\User::first());
});

Route::get('/', 'PagesController@Home')->name('home');
Route::get('pagina/{pagina}','PagesController@showPage')->name('page.single');
Route::get('/blog/{post}', 'PostsController@show')->name('blog.single');
Route::get('/categoria/{category}', 'CategoriesController@show')->name('categories.show');
Route::get('/tag/{tag}', 'TagsController@show')->name('tags.show');
Route::post('/busqueda', 'SearchController@show')->name('search.show');
Route::post('/comments', 'CommentsController@store')->name('comments.store');



Route::group([
	'prefix' => 'admin', 
	'namespace' => 'Admin', 
	'middleware' => 'auth'],
	 function(){
	Route::get('/', 'AdminController@index')->name('admin.dashboard');

	Route::resource('posts', 'PostsController', [
		'as' => 'admin', 
		'except' => 'show'
	]);

	Route::resource('users', 'UsersController', ['as' => 'admin']);
	Route::resource('categories', 'CategoriesController', [
		'as' => 'admin', 
		'except' => 'show'
	]);
	Route::resource('tags', 'TagsController', [
		'as' => 'admin', 
		'except' => 'show'
	]);

	Route::middleware('role:Admin')
			->resource('pages', 'PagesController', [
				'as' => 'admin',
				'except' => 'show',
			]);

	Route::middleware('role:Admin')
			->resource('comments', 'CommentsController', 
				['as' => 'admin', 
				'except' => ['show', 'create', 'store']
			    ]
			);

	Route::middleware('role:Admin')
			->resource('responses', 'ResponsesController', 
				['as' => 'admin', 
				'except' => ['index', 'show', 'create', 'store']
				]
			);



	Route::middleware('role:Admin')
			->patch('comments/{comment}/approve', 'CommentsController@approve')
			->name('admin.comments.approve');

	Route::middleware('role:Admin')
			->patch('comments/{comment}/disapprove', 'CommentsController@disapprove')
			->name('admin.comments.disapprove');

	Route::middleware('role:Admin')
			->patch('responses/{response}/approve', 'ResponsesController@approve')
			->name('admin.responses.approve');

	Route::middleware('role:Admin')
			->patch('responses/{response}/disapprove', 'ResponsesController@disapprove')
			->name('admin.responses.disapprove');
	
	Route::middleware('role:Admin')
	        ->put('/users/{user}/roles', 'UsersRolesController@update')
	        ->name('admin.users.roles.update');

	Route::middleware('role:Admin')
	        ->put('/users/{user}/permissions', 'UsersPermissionsController@update')
	        ->name('admin.users.permissions.update');

/*	Route::middleware('role:Admin')
			->resource('roles', 'RolesController', ['as' => 'admin', 'except' => 'show']);

	Route::middleware('role:Admin')
			->resource('permissions', 'PermissionsController', ['as' => 'admin', 'except' => 'show']);*/		
	
	Route::resource('photos', 'PhotosController', ['as' => 'admin']);
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('confirmation/{id}/{email}', 'Auth\RegisterController@confirmation')->name('account.confirmation');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');



