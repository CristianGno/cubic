<?php

namespace App;

use App\Traits\CommonInModels;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use CommonInModels;

    protected $dates = ['published_at'];
    protected $guarded = [];

    public function responses(){
    	return $this->hasMany(Response::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
    	return $this->belongsTo(Post::class);
    }

    public function setPublishedAtAttribute($published_at){
    	$this->attributes['published_at'] = $published_at == null ? now() : $published_at;
    }

    public function scopeApproved($query, Post $post){
        $q = $query->where('post_id', $post->id);
       return ( auth()->check() && auth()->user()->hasRole('Admin') )
                ? $q
                : $q->where('approved', 1);
    }

    public static function isApproved(Post $post){
        return static::approved($post);
    }
    
}
