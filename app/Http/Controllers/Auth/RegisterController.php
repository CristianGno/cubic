<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserWasConfirmed;
use App\Events\UserWasCreated;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'confirmation_token' => str_random(22),
        ]);

        UserWasCreated::dispatch($user);

        return $user;
    }

   public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        //$this->guard()->login($user);

       return redirect()->route('login')->withFlash('Tu cuenta ha sido creada y ahora debes confirmarla. Revisa tu correo. No olvides revisar tu bandeja de Spam');
    }



    public function confirmation($token, $id){
        if($user = User::find($id)){

            if($user->confirmed != null) {
                return redirect()->route('login')->with('error', 'Tu cuenta ya ha sido confirmada');
            } 
            
            if($user->confirmation_token == $token){
                $user->confirmed = 1;
                $user->confirmation_token = 0;
                $user->update();

                UserWasConfirmed::dispatch($user);

               return redirect()->route('login')->withFlash("Cuenta confirmada correctamente, ahora puedes iniciar sessión");

            } else {
               return redirect()->route('login')->with('error', "Tu cuenta no ha podido ser confirmada o ya está confirmada.");
            }
        } else {

         abort(404, "No se encuentra el usuario");
        }
    }

}
