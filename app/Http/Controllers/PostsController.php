<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\Response;
use App\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show(Post $post){

    	if($post->isPublished() || auth()->check()){
	    	return view('posts.single', [
	    		'title' => $post->title,
	    		'post' => $post,
	    		'populars' => Post::lastFive()->get(),
	    		'comments' => $comments = Comment::isApproved($post)->get(),
	    		'categories' => Category::all(),
	    		'tags' => Tag::all(),
	    	]);
        }

       abort(404, 'Pagina no encontrada');
    }


}
