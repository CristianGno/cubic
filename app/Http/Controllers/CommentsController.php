<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Response;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    protected $guarded = [];

    public function store(Request $request){
    	if(request()->ajax()){
    		if($request->author_name == ''){
    			echo "El campo Nombre es obligatorio";
    			return;
    		}

    		if($request->author_email == ''){
    			echo "El campo Correo es obligatorio";
    			return;
    		}

   	        if($request->body == ''){
    			echo "Escribe un comentario";
    			return;
    		}

    		$request['approved'] = 1;

    		if($request->filled('comment_id')){
	    		Response::create($request->except('post_id'));
    		} else {
    			Comment::create($request->except('comment_id'));
    		}

    		return 'true';

    	}else {
    		return back();
    	}
    }
}
