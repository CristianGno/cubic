<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Response;
use Illuminate\Http\Request;

class ResponsesController extends Controller
{

    public function approve(Response $response){
        $response->update(['approved' => 1]);

        return back()->withFlash("Respuesta Aprobada correctamente.");
    }

    public function disapprove(Response $response){
        $response->update(['approved' => 0]);

        return back()->withFlash("Respuesta desaprobada correctamente.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Response $response)
    {
        $response->delete();

        return back()->withFlash("Respuesta eliminada correctamente.");
    }
}
