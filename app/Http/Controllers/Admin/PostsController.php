<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Photo;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::allowed()->get();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new Post);
        
        return view('admin.posts.create', [
            'title' => "Crear una publicación",
            'categories' => Category::all(),
            'tags' => Tag::all(),
            'images' => Photo::all(),

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    { 
       $request['user_id'] = auth()->user()->id;
       $post = Post::create($request->all());
       $post->storeTags($request->get('tags'), 'attach');

       $post->storePhotos($request, $post);

       return redirect(route('admin.posts.edit', $post))->withFlash('Tu publicación ha sido creada correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        return view('admin.posts.edit', [
            'title' => 'Editando la publicación "' . $post->title . '"',
            'post' => $post,
            'categories' => Category::all(),
            'tags' => Tag::all(),
            'images' => Photo::all(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePostRequest $request, Post $post)
    {
       $this->authorize('update', $post);

       $post->update($request->all());

       $post->storeTags($request->get('tags'), 'sync');

       $post->storePhotos($request, $post);

       return redirect(route('admin.posts.edit', $post))->withFlash('Tu publicación ha sido actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);

        $post->delete();

        return back()->withFlash('La publicación ha sido eliminada correctamente');
    }
}
