<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserWasCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::allowed()->get();
        return view('admin.users.index', compact('users'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new User);

        return view('admin.users.create', [
            'user' => new User,
            'roles' => Role::with('permissions')->get(),
            'permissions' => Permission::pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', new User);

        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:users'
        ]);

        $data['password'] = str_random(8);
        $data['confirmation_token'] = null;
       
        $user = User::create($data);
        
        if($request->filled('roles')){
            $user->assignRole($request->roles);
        }

        if($request->filled('permissions')){
            $user->givePermissionTo($request->permissions);
        }

        
        UserWasCreated::dispatch($user, $data['password']);
        
        return redirect()->route('admin.users.index')->withFlash("El usuario ha sido creado con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('admin.users.edit', [
            'user' => $user,
            'roles' => Role::with('permissions')->get(),
            'permissions' => Permission::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        if($request->file('picture')){
            $this->validate(request(), 
                ['picture' => 'required|image|max:2048']
            );

            if($picture = $user->picture){
                $picture->delete();
            }

            $user->picture()->create([
                'user_id' => $user->id,
                'url' => Storage::url(request()->file('picture')->store('pictures', 'public'))
            ]);
        }

        $user->update( $request->validated() );

        return back()->withFlash('Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        if($picture = $user->picture){
            $picture->delete();
        }

        $user->delete();

        return back()->withFlash("Usuario eliminado correctamente");


    }
}
