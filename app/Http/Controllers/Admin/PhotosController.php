<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Photo;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotosController extends Controller
{
    
    public function store(Request $request){
        if($request->has('images')){
            $this->validate($request, [
                'images' => ['required', 'image', 'max:2048'],
            ]);

            $image = Photo::create([
                'url' => Storage::url($request->file('images')->store('uploads', 'public')),
            ]);

            return url($image->url);
        }
    }


    public function destroy(Photo $photo)
    {
        $photo->delete();

        return back()->withFlash('Thumbnail eliminada correctamente');
    }
}


