<?php

namespace App\Http\Controllers;

use App\Category;
use App\Page;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    
    public function home(){
     return view('home', [
     	'posts' => $posts = Post::published()->paginate(8),
     	'categories' => Category::all(),
     	'tags' => Tag::all()
     ]);
    }

    public function showPage($page){

    	return view('pages.show', [
    		'page' => Page::find($page),
            'posts' => $posts = Post::published()->paginate(8),
            'categories' => Category::all(),
            'tags' => Tag::all()
    	]);
    }

}
