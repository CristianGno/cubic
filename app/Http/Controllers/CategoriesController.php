<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function show(Category $category)
    {
    	return view('home', [
    		'title' => $category->name,
    		'category' => $category,
    		'posts' => $category->posts()->paginate(),
    		'categories' => Category::all(),
     	    'tags' => Tag::all()
    	]);
    }
}
