<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function show(Request $request)
    {
    	$keyword = $request->get('keyword');

       return view('posts.search', [
       	'posts' => $posts = Post::where('title', 'LIKE', "{$keyword}%")->get(),
       	'count' => $posts->count(),
       	'keyword' => $keyword,
       	'title' => 'Resultados de búsqueda',
       ]);
    }
}
