<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function show(Tag $tag){
    	return view('home', [
    		'title' => $tag->name,
    		'posts' => $tag->posts()->paginate(),
    		'categories' => Category::all(),
    		'tags' => Tag::all(),
    	]);
    }
}
