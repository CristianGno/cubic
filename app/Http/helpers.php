<?php 

function siteTitle($title = ''){
	if(request()->routeIs('home')){
		return config('app.name');
	} 
	if($title == ''){
		$title .= 'Página no encontrada';
    }
    return $title . ' | ' . config('app.name');
}

function comments_number($count){
	if($count == 0){
		return 'Sin comentarios <small>Sé el primero en comentar</small>';
	} else if($count == 1){
		return '1 comentario';
	} else{
		return $count . ' comentarios';
	}

	
}

function levels(){
		$levels = explode('/', $_SERVER['REQUEST_URI']);

		unset($levels[0]);
		//unset($levels[1]);

		if(end($levels) === 'edit'){
			unset($levels[count($levels) - 1]);
		}

		?>
		<ol class="breadcrumb">
        <li><a href="<?= route('admin.dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <?php foreach($levels as $level) : 
        	$level = ucfirst($level);
        	if($level == 'Edit' || $level == 'Create'){
        		echo '<li>'.$level.'</li>';
        	} else {
        		?>
				<li><a href="/<?= $level != 'admin' ? 'admin/'.$level : $level ; ?>"><?= $level; ?></a></li>
        	<?php	
        	}
        endforeach; ?>
      </ol>
	
<?php }