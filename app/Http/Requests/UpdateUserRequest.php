<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => [
                'required', 
                 Rule::unique('users')
                          ->ignore($this->route('user')->id)
            ],
            'biography' => 'required|min:20|max:280'
        ];

        if($this->filled('password')){
            $rules['password'] = ['confirmed', 'min:6'];
        }

        if($this->filled('facebook')){
            $rules['facebook'] = ['required', 'min:1'];
        }

        if($this->filled('twitter')){
            $rules['twitter'] = ['required', 'min:1'];
        }

        if($this->filled('github')){
            $rules['github'] = ['required', 'min:1'];
        }

        if($this->filled('google_plus')){
            $rules['google_plus'] = ['required', 'min:1'];
        }

        return $rules;
    }
}
