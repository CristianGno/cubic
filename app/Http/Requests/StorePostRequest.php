<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{

    public $messages = ['category' => "Selecciona una categoría"];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $rules = 
       [
        'title' => 'required|min:6',
        'url' => 'unique:posts',
        'excerpt' => 'required',
        'body' => 'required',
        'category_id' => 'required',
        'tags' => 'required',
        ];

        if($this->has('thumbnail')){
            $rules['thumbnail'] = ['required', 'image', 'max:2048'];
        }

        return $rules;
    }

    public function messages()
    {
        return [];
    }
   
}
