<?php 

namespace App\Traits;

trait CommonInModels
{

    public function setNameAttribute($name){
    	$this->attributes['name'] = $name;
    	$this->attributes['url'] = str_slug($name);    	
    }

   public function setTitleAttribute($title){
        $this->attributes['title'] = $title;
        $this->attributes['url'] = $this->generateUrl($title);     
    }

    public static function getLimits($limit = 1){
    	return static::limit($limit);
    }

    public static function lastTen() {
    	return static::getLimits(10);
    }

    public static function lastFive() {
    	return static::getLimits(5);
    }

    public function getRouteKeyName(){
        return 'url';
    }

    private function generateUrl($title){
        $url = str_slug($title);
/*
        if($this->where('url', $url)->exists()){
            $post = $this::create(['title' => $title])
            $url = "{$url}-{$this->id}";
        }*/

        return $url;
    }

}