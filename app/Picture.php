<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Picture extends Model
{
    protected $guarded = [];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public static function boot(){
    	parent::boot();

    	static::deleting(function($picture){
    		$path = str_replace('storage', 'public', $picture->url);
    		Storage::delete($path);
    	});
    }
}
