<?php

namespace App;

use App\Traits\CommonInModels;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $dates = ['published_at'];
    protected $guarded = [];

    public function comment(){
    	return $this->belongsTo(Comment::class);
    }

    public function setPublishedAtAttribute($published_at){
    	$this->attributes['published_at'] = $published_at == null ? now() : $published_at;
    }

    public function scopeApproved($query, Comment $comment){
       return $query->where('comment_id', $post->id)
                ->where('approved', 1);
    }

    public static function isApproved(Response $response){
        return $response->approved == 1 ? true : false;
    }
}