<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use App\Mail\EmailConfirmation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailConfirmation 
{

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        Mail::to($event->user)
                ->queue(new EmailConfirmation($event->user));
    }
}
