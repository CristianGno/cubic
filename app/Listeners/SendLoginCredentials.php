<?php

namespace App\Listeners;

use App\Events\UserWasConfirmed;
use App\Mail\LoginCredentials;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendLoginCredentials
{

    /**
     * Handle the event.
     *
     * @param  UserWasConfirmed  $event
     * @return void
     */
    public function handle(UserWasConfirmed $event)
    {
        Mail::to($event->user)
               ->queue(new LoginCredentials($event->user));
    }
}
