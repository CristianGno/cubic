<?php

namespace App;

use App\Traits\CommonInModels;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{

    use CommonInModels;

    protected $dates = ['published_at'];

    protected $fillable = [
       'title', 'excerpt', 'body', 'published_at', 'category_id', 'user_id', 
    ];

    public function category($value= ""){
    	return $this->belongsTo(Category::class);
    }

    public function tags(){
    	return $this->belongsToMany(Tag::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function photos(){
        return $this->hasMany(Photo::class);
    }

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }


    public function scopePublished($query){
    	$query->whereNotNull('published_at')
     		  ->where('published_at', '<=', Carbon::now())
              ->orderBy('id', 'desc');
    }

    public function scopeAllowed($query){
        return auth()->user()->can('view', $this) 
                  ? $query 
                  : $query->where('user_id', auth()->user()->id);
    }

 

    public function setPublishedAtAttribute($published_at){
        $this->attributes['published_at'] = $published_at ? Carbon::parse($published_at) : null;
    }

    public function setCategoryIdAttribute($category){
        $this->attributes['category_id'] = Category::find($category) 
                                            ? $category 
                                            : Category::create(['name' => $category])->id; 
    }

    public function storeTags($tags, $method = 'sync'){
      
      $tagsIds = collect($tags)->map(function($tag){
         return Tag::find($tag) ? $tag : Tag::create(['name' => $tag])->id;
      }); 

       return $this->tags()->$method($tagsIds);        
    }

    protected static function boot(){
        parent::boot();

        static::deleting(function($post){
           $post->tags()->detach();
           $post->photos->each->delete();
        });
    }

    public function isPublished(){
        return ! is_null($this->published_at) && $this->published_at < today();
    }

    public function storePhotos($request, $post){
       if($request->has('thumbnail')){
            $post->photos()->create([
               'url' => Storage::url($request->file('thumbnail')->store('posts', 'public')),
            ]);
        }
    }



}
