<?php

namespace App;

use App\Traits\CommonInModels;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	use CommonInModels;

    protected $guarded = [];

    public function getRouteKeyName(){
    	return 'id';
    }
}
