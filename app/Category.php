<?php

namespace App;

use App\Traits\CommonInModels;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use CommonInModels;
    
    protected $guarded = [];



    public function posts(){
    	return $this->hasMany(Post::class);
    }

    public function post(){
    	return $this->belongsTo(Post::class);
    }

    public static function boot(){
    	parent::boot();

    	static::deleting(function($category){
    		$category->posts()->update(['category_id' => 0]);
    	});
    }

}
