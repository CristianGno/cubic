<?php

namespace App;

use App\Picture;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }

    public function setConfirmationTokenAttribute($confirmation_token){
        $this->attributes['confirmation_token'] = (empty($confirmation_token) || is_null($confirmation_token))
                                                ? str_random(22)
                                                : $confirmation_token;
    }

    public function scopeAllowed($query){
        return auth()->user()->can('view', $this) 
                  ? $query 
                  : $query->where('id', auth()->user()->id);
    }

    public function picture(){
        return $this->hasOne(Picture::class);
    }
}
