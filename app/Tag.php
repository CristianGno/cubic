<?php

namespace App;

use App\Traits\CommonInModels;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use CommonInModels;
    
    protected $guarded = [];
    

    public function posts(){
    	return $this->belongsToMany(Post::class);
    }

    public static function boot(){
    	parent::boot();

    	static::deleting(function($tag){
    		$tag->posts()->detach();
    	});
    }

}
